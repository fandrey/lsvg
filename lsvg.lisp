;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Lsvg.
;;;
;;; Lsvg is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Lsvg is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lsvg.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:lsvg)

(defun make-shader (type shader)
  "Compile SHADER of TYPE."
  (let ((sh (gl:create-shader type)))
    (if (= sh 0)
        (error "A shader of type ~A is not created" type))
    (gl:shader-source sh shader)
    (gl:compile-shader sh)
    (unless (gl:get-shader sh :compile-status)
      (error "Shader compilation failed.  The info log is:~%~A"
             (gl:get-shader-info-log sh)))
    sh))

(defun make-shaders (vertex-shader fragment-shader)
  "Make a shader program from VERTEX-SHADER and FRAGMENT-SHADER.
VERTEX-SHADER and FRAGMENT-SHADER are strings of shaders sources."
  (let ((pr (gl:create-program)))
    (if (= pr 0)
        (error "A program is not created"))
    (gl:attach-shader pr (make-shader :vertex-shader vertex-shader))
    (gl:attach-shader pr (make-shader :fragment-shader fragment-shader))
    (gl:link-program pr)
    (unless (gl:get-program pr :link-status)
      (error "Program linking failed.  The info log is:~%~A"
             (gl:get-program-info-log pr)))
    pr))

(defun free-shader-program (program)
  "Delete the shader program PROGRAM."
  (mapc #'gl:delete-shader (gl:get-attached-shaders program))
  (gl:delete-program program))

(defun load-shaders (vertex-shader fragment-shader &rest attrib-locations)
  (let ((program (make-shaders vertex-shader fragment-shader))
        (locations))
    (unwind-protect-case ()
         (dolist (attr attrib-locations)
           (push (gl:get-attrib-location program attr) locations))
      (:abort (free-shader-program program)))
    (apply #'values (cons program (nreverse locations)))))

(defmacro with-gl-array ((var type count) &body body)
  `(let ((,var (gl:alloc-gl-array ,type ,count)))
     (unwind-protect
          (progn ,@body)
       (gl:free-gl-array ,var))))

(defun init-render ()
  (multiple-value-bind (program position texposition)
      (load-shaders "attribute vec2 position;
                     attribute vec2 texposition;
                     uniform mat3 view;
                     varying vec2 texcoord;
                     void main()
                     {
                       gl_Position = vec4(view * vec3(position, 0.), 1.);
                       texcoord = texposition;
                     }"
                    "varying vec2 texcoord;
                     uniform sampler2D sampler;
                     void main()
                     {
                       gl_FragColor = texture2D(sampler, texcoord);
                     }"
                    "position" "texposition")
    (let* ((view (gl:get-uniform-location program "view"))
           (vertex-array 0)
           (vertex-buffer 0)
           (texture 0)
           (free-render (lambda ()
                          (when program
                            (free-shader-program program))
                          (unless (zerop vertex-buffer)
                            (gl:delete-buffers (list vertex-buffer)))
                          (unless (zerop vertex-array)
                            (gl:delete-vertex-arrays (list vertex-array)))
                          (unless (zerop texture)
                            (gl:delete-texture texture))))
           (load-texture (lambda (rgba w h)
                           (gl:bind-texture :texture-2d texture)
                           (gl:tex-parameter :texture-2d
                                             :texture-mag-filter :linear)
                           (gl:tex-parameter :texture-2d
                                             :texture-min-filter :linear)
                           (gl:tex-image-2d :texture-2d 0 :rgba w h 0
                                            :bgra :unsigned-byte rgba)))
           (render (lambda ()
                     (gl:clear :color-buffer)
                     (gl:use-program program)
                     (gl:uniform-matrix-3fv view
                                            #(-1f0 0f0 0f0
                                              0f0 1f0 0f0
                                              0f0 0f0 1f0))
                     (gl:bind-vertex-array vertex-array)
                     (gl:bind-buffer :array-buffer vertex-buffer)
                     (gl:bind-texture :texture-2d texture)
                     (gl:draw-arrays :triangle-strip 0 4)
                     (swap-buffers))))
      (unwind-protect-case ()
           (progn
             (when (zerop (setq vertex-array (gl:gen-vertex-array)))
               (error "Can't generate a vertex array"))
             (when (zerop (setq vertex-buffer (car (gl:gen-buffers 1))))
               (error "Can't generate a vertex buffer"))
             (when (zerop (setq texture (car (gl:gen-textures 1))))
               (error "Can't generate a texture"))
             (with-gl-array (ar :float (* 4 2 2))
               (setf (glaref ar  0)  1f0
                     (glaref ar  1) -1f0
                     (glaref ar  2) -1f0
                     (glaref ar  3) -1f0
                     (glaref ar  4)  1f0
                     (glaref ar  5)  1f0
                     (glaref ar  6) -1f0
                     (glaref ar  7)  1f0
                     (glaref ar  8)  0f0
                     (glaref ar  9)  1f0
                     (glaref ar 10)  1f0
                     (glaref ar 11)  1f0
                     (glaref ar 12)  0f0
                     (glaref ar 13)  0f0
                     (glaref ar 14)  1f0
                     (glaref ar 15)  0f0)
               (gl:bind-vertex-array vertex-array)
               (gl:bind-buffer :array-buffer vertex-buffer)
               (gl:buffer-data :array-buffer :static-draw ar)
               (gl:vertex-attrib-pointer position 2 :float nil 0 (null-pointer))
               (gl:vertex-attrib-pointer
                texposition 2 :float nil 0
                (inc-pointer (null-pointer)
                             (* 4 2 (foreign-type-size :float)))))
             (gl:enable-vertex-attrib-array position)
             (gl:enable-vertex-attrib-array texposition))
        (:abort (funcall free-render)))
      (values render load-texture free-render))))

(def-window-size-callback update-viewport (window w h)
  (declare (ignore window))
  (gl:viewport 0 0 w h))

(defvar *lsvg-thread* nil)
(defvar *lsvg-channel* nil)

(defun svg-dimensions (data data-len)
  (cairo:with-surface-and-context
      (nil (cairo:create-image-surface :argb32 1 1))
    (rsvg2:with-handle-from-data (svg data data-len)
      (rsvg2:handle-get-dimension-values svg))))

(defun define-render-svg (load-texture-function)
  (defun render-svg (svg &optional (input-type :lxml) (ignore-namespaces t))
    (with-foreign-string
        ((data data-len)
         (if (stringp svg)
             svg
             (let ((s-xml:*ignore-namespaces* ignore-namespaces))
               (s-xml:print-xml-string svg :input-type input-type)))
         :null-terminated-p nil)
      (multiple-value-bind (width height) (svg-dimensions data data-len)
        (cairo:with-surface-and-context
            (nil (cairo:create-image-surface :argb32 width height))
          (rsvg2:draw-svg-data data data-len)
          (let* ((rgba (cairo:image-surface-get-data cairo:*surface*))
                 (ldtex (lambda ()
                          (funcall load-texture-function
                                   rgba width height))))
            (if (eq (bt:current-thread) *lsvg-thread*)
                (funcall ldtex)
                (sendmsg *lsvg-channel* ldtex))))))))

(defun lsvg ()
  (unless (and (bt:threadp *lsvg-thread*) (bt:thread-alive-p *lsvg-thread*))
    (bt:make-thread
     (lambda ()
       (setq *lsvg-thread* (bt:current-thread)
             *lsvg-channel* (make-channel))
       (let ((w 1024)
             (h 768))
         (with-init-window (:title "Lsvg" :width w :height h)
           (setf %gl:*gl-get-proc-address* #'get-proc-address)
           (set-window-size-callback 'update-viewport)
           (gl:clear-color 0 0 0 0)
           (gl:viewport 0 0 w h)
           (multiple-value-bind (render load-texture free-render)
               (init-render)
             (define-render-svg load-texture)
             (unwind-protect
                  (loop :until (window-should-close-p)
                        :do (progn
                              (if (hasmsg *lsvg-channel*)
                                  (funcall (recvmsg *lsvg-channel*)))
                              (funcall render)
                              (poll-events)))
               (funcall free-render))))))
     :name "lsvg-thread")))

