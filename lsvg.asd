;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Lsvg.
;;;
;;; Lsvg is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Lsvg is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lsvg.  If not, see <http://www.gnu.org/licenses/>.

(asdf:defsystem #:lsvg
  :description "Lsvg -- generate SVG from Lisp and draw with OpenGL"
  :author "Andrey Fainer <fandrey@gmx.com>"
  :license "GPLv3"
  :serial t
  :depends-on (:bordeaux-threads
               :cl-opengl
               :cl-glfw3
               :cl-rsvg2
               :s-xml
               :trivial-channels)
  :components ((:file "package")
               (:file "lsvg")))
