;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Lsvg.
;;;
;;; Lsvg is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Lsvg is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Lsvg.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:lsvg
  (:use #:cl #:cl-glfw3 #:trivial-channels)
  (:import-from #:alexandria
                #:unwind-protect-case)
  (:import-from #:cffi
                #:inc-pointer
                #:null-pointer
                #:foreign-type-size
                #:mem-ref
                #:with-foreign-string)
  (:import-from #:gl
                #:glaref)
  (:export #:lsvg
           #:render-svg))

(defpackage #:lsvg-user
  (:use #:cl #:lsvg))
